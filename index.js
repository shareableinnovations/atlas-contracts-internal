const bankCodes = [
  '001','001-2', '037', '930', '012', '504', '039'
];

const accountDef = {
  type: "object",
  properties: {
    number: { type: "string" },
    name: { type: "string" },
    currency: { type: "string" },
    type: { type: "string" },
    username: {
      type: "string",
      pattern: '^\\d{1,2}\\.\\d{3}\\.\\d{3}-(\\d|K){1}$'
    },
    businessId: {
      type: "string"
    },
    bank: {
      type: "string",
      enum: bankCodes
    }
  },
  required: ['number', 'name', 'currency', 'type', 'username', 'bank']
};


module.exports.amqp = {

  SCRAPE_PAYEES: {
    properties: {
      username: {
        type: 'string', // A RUT passed on as a string
        pattern: '^\\d{1,2}\\.\\d{3}\\.\\d{3}-(\\d|K){1}$'
      },
      password: {
        type: 'string'
      },
      bankCode: {
        type: 'string',
        enum: bankCodes
      },
      accountType: {
        type: 'string',
        enum: ['person', 'business']
      },
      twoFactorCode: {
        type: 'string'
      },
      token: {
        type: 'string',
        minLength: 100 // Tokens vary in size but are never smaller than this
      },
      callback: {
        type: 'string',
        format: 'uri'
      }
    },
    required: ['username', 'password', 'bankCode', 'accountType', 'token', 'callback']
  },

  SCRAPE_ACCOUNTS_RESULT: {
    properties:{
      accounts:{
        type: "array",
        items:{
          type: "object",
          properties: {
            number: { type: "string" },
            name: { type: "string" },
            currency: { type: "string" },
            type: { type: "string" },
            username: {
              type: "string",
              pattern: '^\\d{1,2}\\.\\d{3}\\.\\d{3}-(\\d|K){1}$'
            },
            bank: {
              type: "string",
              enum: bankCodes
            }
          },
          required: ['number', 'name', 'currency', 'type', 'username', 'bank']
        }
      },
    },
    required: ['accounts']
  },

  SCRAPE_ERROR_RESULT:{
    properties: {
      error:{
        type: ["object", "string"]
      }
    },
    required: ['error']
  },

  SCRAPE_STATEMENT_RESULT:{
    properties: {
      account: accountDef,
      transactions: {
        type: "array",
        items: {
          type: "object",
          properties: {
            date: { type: "string" },
            type: { type: "string" },
            amount: { type: "number" },
            statement: { type: ["number", "string"]},
            description: { type: "string" },
            identifier: { type: "string" },
            image: { type: "string" }
          },
          required: ['date', 'type', 'amount', 'description']
        }
      },
      balances: {
        type: "array",
        items: {
          type: "object",
          properties: {
            availableBalance: { type: "number" },
            finalBalance: { type: "number" },
            retentions: { type: "number" },
            periodTo: { type: "string" },
            periodFrom: { type: "string" },
            totalCredits: { type: "number" },
            totalDebits: { type: "number" },
            lastBalance: { type: "number" }
          },
          required: ['availableBalance', 'finalBalance', 'retentions', 'periodTo', 'periodFrom' ,'totalCredits', 'totalDebits', 'lastBalance']
        }
      }
    },
    required: ['account', 'transactions', 'balances']
  },

  SCRAPE_PAYMENTS_RECEIVED_RESULT:{
    properties: {
      account: accountDef,
      payments:{
        type: "array",
        items: {
          type: "object",
          properties: {
            date: { type: "number" },
            issuerId: { type: "string" },
            issuerName: { type: "string" },
            description: { type: "string" },
            sourceAccount: { type: "string" },
            amount: { type: "number" }
          },
          required: ['date', 'issuerId', 'issuerName', 'description', 'sourceAccount', 'amount']
        }
      },
    },
    required: ['account', 'payments']
  },

  SCRAPE_PAYMENTS_AUTOMATIC_RESULT:{
    properties: {
      account: accountDef,
      pacs:{
        type: "array",
        items: {
          type: "object",
          properties: {
            date: { type: "number" },
            issuerName: { type: "string" },
            clientId: { type: "string" },
            amount: { type: "number" }
          },
          required: ['date', 'issuerName', 'clientId', 'amount']
        }
      },
    },
    required: ['account', 'pacs']
  },

  SCRAPE_PAYMENTS_INTERNET_RESULT:{
    properties: {
      account: accountDef,
      payments:{
        type: "array",
        items: {
          type: "object",
          properties: {
            date: { type: "number" },
            issuerId: { type: "string" },
            issuerName: { type: "string" },
            clientId: { type: "string" },
            amount: { type: "number" }
          },
          required: ['date', 'issuerId', 'issuerName', 'clientId', 'amount']
        }
      },
    },
    required: ['account', 'payments']
  },

};
